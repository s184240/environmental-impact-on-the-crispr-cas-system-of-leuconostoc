#! usr/bin/env python3

import argparse

parser = argparse.ArgumentParser(description="Infiles")

parser.add_argument('-i', '--infile', help=("Input files"), nargs='+', metavar='In.fasta', default=None)
args = parser.parse_args()


outfile = open('SampleTree.fasta','a')

for i in range(len(args.infile)):
    infile = open(args.infile[i],'r')
    for line in infile:
        print(line[:-1],file=outfile)
    infile.close()

outfile.close()
