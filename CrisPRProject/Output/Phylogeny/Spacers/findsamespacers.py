#!/usr/bin/env python3

import sys
import re

infile = open(sys.argv[1],'r')
outfile = open('DairySpacers.fasta','w')

Spacerdict = dict()

for line in infile:
    if line.startswith('>'):
        ID = line[:-1]
    elif re.search(r'^\d',line) is not None:
        pass
    else:
        Spacer = line[:-1]
        if Spacer in Spacerdict:
            Spacerdict[Spacer] += str('_'+ID[1:])
        else:
            Spacerdict[Spacer] = ID

infile.close()

for Spacer in Spacerdict:
    print(Spacerdict[Spacer],file=outfile)
    print(Spacer, file=outfile)

outfile.close()
