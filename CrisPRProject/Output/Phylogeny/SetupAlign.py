#! usr/bin/env python3

import argparse
import sys
import re

parser = argparse.ArgumentParser(description="Infiles")

parser.add_argument('-i', '--infile', help=("Input files"), nargs='+', metavar='In.fasta', default=None)
args = parser.parse_args()

infile = open(args.infile[0],'r')
outfile = open('DairySpacers.fasta','w')
i=1

for line in infile:
    if line.startswith('>'):
       ID = line
    elif re.search('\d',line) != None:
       i = 1
    else:
       print(ID[:-1]+'_Dairy_'+str(i),file=outfile)
       print(line[:-1],file=outfile)
       i += 1
infile.close()
outfile.close()
