#usr/bin/env python3

import sys
import os
import argparse

def readspacer(Input,Output,ID):
    infile=open(Input,'r')
    ID = ID.split('/')[-2]
    ID = ID[7:-19]
    print('>'+ID, file=Output)
    i=0
    j=0
    for line in infile:
        if line.startswith('"Type": "Spacer",'):
           i += 1
           j += 1
        if i == 4:
           line = line.split(' ')[1]
           line = line[1:-2]
           print(line, file=Output)
           i = 0
        if i > 0:
           i += 1
    print(j, file=Output)
    infile.close()
    return(None)

outfile=open('DairySpacers.txt', 'w')

parser = argparse.ArgumentParser(description="Infiles")

parser.add_argument('-i', '--infile', help=("Input files"), nargs='+', metavar='In.fasta', default=None)
args = parser.parse_args()

for i in range(len(args.infile)):
    readspacer(args.infile[i],outfile,args.infile[i])
outfile.close()
