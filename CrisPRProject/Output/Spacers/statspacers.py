#usr/bin/env python3

import argparse
import sys
import re

parser = argparse.ArgumentParser(description="Infiles")

parser.add_argument('-i', '--infile', help=("Input files"), nargs='+', metavar='In.fasta', default=None)
args = parser.parse_args()

infile = open(args.infile[0],'r')

spacers=list()
nbspacers=list()

samples=0

for line in infile:
    if line.startswith('>'):
       samples += 1
    elif re.search('\d',line) != None:
       nbspacers.append(line[:-1])
    else:
       spacers.append(line[:-1])

infile.close()

avg = 0

for spacer in spacers:
    avg += len(spacer)

avg = avg/len(spacers)

lenghts = list()

for spacer in spacers:
    lenghts.append(len(spacer))



print('Average spacers found: '+str(len(spacers)/samples))
print('Average spacer length: '+ str(avg))
print(lenghts)
print(nbspacers)

