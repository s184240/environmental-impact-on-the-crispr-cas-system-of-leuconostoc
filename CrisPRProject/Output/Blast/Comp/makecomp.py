#!/usr/bin/env python3

import argparse
import sys

parser = argparse.ArgumentParser(description="Infiles")

parser.add_argument('-i', '--infile', help=("Input files"), nargs='+', metavar='In.fasta', default=None)
args = parser.parse_args()

trans_table=str.maketrans('ATCG','TAGC')

infile = open(args.infile[0],'r')
outfile= open('AllSpacersComp.fasta','w')

for line in infile:
    if line.startswith('>'):
        print(line[:-1],file=outfile)
    else:
        line=line[:-1]
        line=line.translate(trans_table)
        print(line,file=outfile)

infile.close()
outfile.close()
