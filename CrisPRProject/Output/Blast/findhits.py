#!/usr/bin/env python3

import argparse
import subprocess
import sys

parser = argparse.ArgumentParser(description="Infiles")

parser.add_argument('-i', '--infile', help=("Input files"), nargs='+', metavar='In.fasta', default=None)
args = parser.parse_args()

infile = open(args.infile[0],'r')
outfile = open('Blastfindings.txt','w')
flag=False
i=0

for line in infile:
    if line.startswith('Sequences producing significant alignments'):
        flag = True
    elif line.startswith('\n') and flag == True:
        i += 1
    elif flag == True:
        print(line[:-1],file=outfile)
    if i >= 2:
        i = 0
        flag = False

infile.close()
outfile.close()
