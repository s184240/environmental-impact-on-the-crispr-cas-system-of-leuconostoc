#!/usr/bin/env python3

import argparse
import subprocess
import sys

parser = argparse.ArgumentParser(description="Infiles")

parser.add_argument('-i', '--infile', help=("Input files"), nargs='+', metavar='In.fasta', default=None)
args = parser.parse_args()

hitdict=dict()

infile = open(args.infile[0],'r')

for line in infile:
    if line[:-16] in hitdict:
        hitdict[line[:-16]] += 1
    else:
        hitdict[line[:-16]] = 1

infile.close()

outfile = open('BlastfindClean.txt','w')

sortedhits= sorted(hitdict.keys(), key=hitdict.get, reverse=True)

for hit in sortedhits:
    print(hit,file=outfile)
    print(hitdict[hit],file=outfile)

outfile.close()    
