AP017936.1 Leuconostoc mesenteroides DNA, complete genome, strain...  56.5    2e-05
CP015442.1 Leuconostoc mesenteroides subsp. mesenteroides strain ...  56.5    2e-05
CP042393.1 Leuconostoc citreum strain CBA3623 chromosome, complet...  148     6e-32
CP015442.1 Leuconostoc mesenteroides subsp. mesenteroides strain ...  148     6e-32
DQ489736.1 Leuconostoc citreum KM20, complete genome                  148     6e-32
AP017936.1 Leuconostoc mesenteroides DNA, complete genome, strain...  143     3e-30
CP020731.1 Leuconostoc mesenteroides subsp. mesenteroides FM06, c...  143     3e-30
CP014610.1 Leuconostoc mesenteroides subsp. mesenteroides strain ...  143     3e-30
CP042413.1 Leuconostoc citreum strain CBA3624 chromosome, complet...  141     1e-29
CP017197.1 Leuconostoc gelidum subsp. gasicomitatum strain TMW 2....  137     1e-28
AP017935.1 Leuconostoc mesenteroides subsp. suionicum DNA, comple...  137     1e-28
CP015247.1 Leuconostoc mesenteroides subsp. suionicum strain DSM ...  137     1e-28
CP035271.1 Leuconostoc mesenteroides strain SRCM103453 chromosome...  126     3e-25
CP021966.1 Leuconostoc mesenteroides strain CBA7131 chromosome, c...  126     3e-25
CP046149.1 Leuconostoc citreum strain WiKim0101 chromosome            113     2e-21
AP017936.1 Leuconostoc mesenteroides DNA, complete genome, strain...  67.6    2e-08
BK021896.1 TPA: Siphoviridae sp. isolate ct6uq2, partial genome       56.5    2e-05
CP065975.1 Leuconostoc pseudomesenteroides strain FDAARGOS_1004 c...  56.5    2e-05
CP002898.1 Leuconostoc sp. C2, complete genome                        56.5    2e-05
CP017197.1 Leuconostoc gelidum subsp. gasicomitatum strain TMW 2....  56.5    2e-05
CP065975.1 Leuconostoc pseudomesenteroides strain FDAARGOS_1004 c...  56.5    2e-05
CP063830.1 Leuconostoc citreum strain 37 chromosome, complete genome  56.5    2e-05
BK021896.1 TPA: Siphoviridae sp. isolate ct6uq2, partial genome       52.8    2e-04
CP065975.1 Leuconostoc pseudomesenteroides strain FDAARGOS_1004 c...  75.0    1e-10
CP017197.1 Leuconostoc gelidum subsp. gasicomitatum strain TMW 2....  54.7    3e-04
CP065975.1 Leuconostoc pseudomesenteroides strain FDAARGOS_1004 c...  52.8    0.001
CP063830.1 Leuconostoc citreum strain 37 chromosome, complete genome  52.8    0.001
BK021896.1 TPA: Siphoviridae sp. isolate ct6uq2, partial genome       56.5    2e-05
CP065975.1 Leuconostoc pseudomesenteroides strain FDAARGOS_1004 c...  56.5    2e-05
CP002898.1 Leuconostoc sp. C2, complete genome                        56.5    2e-05
CP028256.1 Leuconostoc mesenteroides strain SRCM102735 plasmid un...  56.5    2e-05
CP028253.1 Leuconostoc mesenteroides strain SRCM102733 plasmid un...  56.5    2e-05
CP028252.1 Leuconostoc mesenteroides strain SRCM102733 plasmid un...  56.5    2e-05
CP046151.1 Leuconostoc citreum strain WiKim0101 plasmid unnamed2      56.5    2e-05
CP046150.1 Leuconostoc citreum strain WiKim0101 plasmid unnamed1      56.5    2e-05
CP046064.1 Leuconostoc mesenteroides subsp. mesenteroides strain ...  56.5    2e-05
CP046063.1 Leuconostoc mesenteroides subsp. mesenteroides strain ...  56.5    2e-05
CP017199.1 Leuconostoc gelidum subsp. gasicomitatum strain TMW 2....  56.5    2e-05
CP017198.1 Leuconostoc gelidum subsp. gasicomitatum strain TMW 2....  56.5    2e-05
CP042419.1 Leuconostoc citreum strain CBA3627 plasmid unnamed, co...  56.5    2e-05
CP042384.1 Leuconostoc pseudomesenteroides strain CBA3630 plasmid...  56.5    2e-05
CP042421.1 Leuconostoc lactis strain CBA3622 plasmid unnamed1, co...  56.5    2e-05
CP042411.1 Leuconostoc citreum strain CBA3621 plasmid unnamed, co...  56.5    2e-05
CP042398.1 Leuconostoc citreum strain CBA3623 plasmid unnamed5, c...  56.5    2e-05
CP042394.1 Leuconostoc citreum strain CBA3623 plasmid unnamed1, c...  56.5    2e-05
CP042406.1 Leuconostoc mesenteroides strain CBA3628 plasmid unnam...  56.5    2e-05
CP042405.1 Leuconostoc mesenteroides strain CBA3628 plasmid unnam...  56.5    2e-05
CP042376.1 Leuconostoc carnosum strain CBA3620 plasmid unnamed2, ...  56.5    2e-05
CP042375.1 Leuconostoc carnosum strain CBA3620 plasmid unnamed1, ...  56.5    2e-05
CP042417.1 Leuconostoc citreum strain CBA3624 plasmid unnamed4, c...  56.5    2e-05
CP042416.1 Leuconostoc citreum strain CBA3624 plasmid unnamed3, c...  56.5    2e-05
CP042414.1 Leuconostoc citreum strain CBA3624 plasmid unnamed1, c...  56.5    2e-05
CP037938.1 Leuconostoc kimchii strain NKJ218 plasmid pLKN1, compl...  56.5    2e-05
CP037936.1 Leuconostoc kimchii strain NKJ218 plasmid unnamed          56.5    2e-05
CP035747.1 Leuconostoc mesenteroides strain SRCM103460 plasmid un...  56.5    2e-05
CP035272.1 Leuconostoc mesenteroides strain SRCM103453 plasmid un...  56.5    2e-05
CP035141.1 Leuconostoc mesenteroides strain SRCM103356 plasmid un...  56.5    2e-05
CP021968.1 Leuconostoc mesenteroides strain CBA7131 plasmid pLMCB...  56.5    2e-05
CP021967.1 Leuconostoc mesenteroides strain CBA7131 plasmid pLMCB...  56.5    2e-05
LT996083.1 Leuconostoc carnosum strain MFPA29A1405 genome assembl...  56.5    2e-05
CP024927.1 Leuconostoc citreum strain EFEL 2700 plasmid pCB205, c...  56.5    2e-05
CP024925.1 Leuconostoc citreum strain EFEL 2700 plasmid pCB433        56.5    2e-05
CP021492.1 Leuconostoc mesenteroides strain WiKim33 plasmid unnam...  56.5    2e-05
AP017937.1 Leuconostoc mesenteroides plasmid pLm151-a DNA, comple...  56.5    2e-05
CP020732.1 Leuconostoc mesenteroides subsp. mesenteroides FM06 pl...  56.5    2e-05
CP014602.1 Leuconostoc mesenteroides subsp. mesenteroides strain ...  56.5    2e-05
CP013017.1 Leuconostoc mesenteroides subsp. mesenteroides strain ...  56.5    2e-05
CP014613.1 Leuconostoc mesenteroides strain DRC1506 plasmid pDRC2...  56.5    2e-05
CP014612.1 Leuconostoc mesenteroides strain DRC1506 plasmid pDRC1...  56.5    2e-05
LN890332.1 Leuconostoc gelidum subsp. gasicomitatum KG16-1 genome...  56.5    2e-05
CP003103.2 Leuconostoc mesenteroides subsp. mesenteroides J18 pla...  56.5    2e-05
CP003102.2 Leuconostoc mesenteroides subsp. mesenteroides J18 pla...  56.5    2e-05
CP037753.1 Leuconostoc mesenteroides strain WiKim32 plasmid unnam...  56.5    2e-05
CP037750.1 Leuconostoc mesenteroides strain WiKim32 plasmid unnam...  56.5    2e-05
CP070516.1 Leuconostoc falkenbergense strain FDAARGOS_1201 plasmi...  56.5    2e-05
CP003854.1 Leuconostoc carnosum JB16 plasmid pKLC3, complete genome   56.5    2e-05
CP003853.1 Leuconostoc carnosum JB16 plasmid pKLC2, complete genome   56.5    2e-05
CP003852.1 Leuconostoc carnosum JB16 plasmid pKLC1, complete genome   56.5    2e-05
CP066297.1 Leuconostoc citreum strain WiKim0096 plasmid unnamed1      56.5    2e-05
CP065996.1 Leuconostoc mesenteroides strain FDAARGOS_1033 plasmid...  56.5    2e-05
CP065977.1 Leuconostoc pseudomesenteroides strain FDAARGOS_1004 p...  56.5    2e-05
CP065973.1 Leuconostoc pseudomesenteroides strain FDAARGOS_1004 p...  56.5    2e-05
CP063831.1 Leuconostoc citreum strain 37 plasmid unnamed, complet...  56.5    2e-05
CP039737.1 Leuconostoc sp. LN180020 plasmid unnamed2, complete se...  56.5    2e-05
CP001756.1 Leuconostoc kimchii IMSNU 11154 plasmid LkipL4726, com...  56.5    2e-05
DQ489740.1 Leuconostoc citreum KM20 plasmid pLCK1, complete sequence  56.5    2e-05
DQ489737.1 Leuconostoc citreum KM20 plasmid pLCK2, complete sequence  56.5    2e-05
CP000415.1 Leuconostoc mesenteroides subsp. mesenteroides ATCC 82...  56.5    2e-05
CP035274.1 Leuconostoc mesenteroides strain SRCM103453 plasmid un...  52.8    2e-04
CP065975.1 Leuconostoc pseudomesenteroides strain FDAARGOS_1004 c...  56.5    2e-05
CP020731.1 Leuconostoc mesenteroides subsp. mesenteroides FM06, c...  56.5    2e-05
BK021896.1 TPA: Siphoviridae sp. isolate ct6uq2, partial genome       56.5    2e-05
CP070515.1 Leuconostoc falkenbergense strain FDAARGOS_1201 chromo...  56.5    2e-05
CP065975.1 Leuconostoc pseudomesenteroides strain FDAARGOS_1004 c...  56.5    2e-05
CP017197.1 Leuconostoc gelidum subsp. gasicomitatum strain TMW 2....  54.7    6e-05
CP063830.1 Leuconostoc citreum strain 37 chromosome, complete genome  54.7    6e-05
CP065975.1 Leuconostoc pseudomesenteroides strain FDAARGOS_1004 c...  56.5    2e-05
MN855637.1 Bacteriophage sp. isolate 32, complete genome              56.5    2e-05
CP042383.1 Leuconostoc pseudomesenteroides strain CBA3630 chromos...  56.5    2e-05
CP021491.1 Leuconostoc mesenteroides strain WiKim33 chromosome, c...  56.5    2e-05
CP065993.1 Leuconostoc pseudomesenteroides strain FDAARGOS_1003 c...  56.5    2e-05
NC_055037.1 Leuconostoc phage phiMH1, partial genome                  56.5    2e-05
CP017197.1 Leuconostoc gelidum subsp. gasicomitatum strain TMW 2....  56.5    2e-05
CP065975.1 Leuconostoc pseudomesenteroides strain FDAARGOS_1004 c...  56.5    2e-05
CP063830.1 Leuconostoc citreum strain 37 chromosome, complete genome  56.5    2e-05
BK021896.1 TPA: Siphoviridae sp. isolate ct6uq2, partial genome       52.8    2e-04
AP017936.1 Leuconostoc mesenteroides DNA, complete genome, strain...  67.6    2e-08
CP065975.1 Leuconostoc pseudomesenteroides strain FDAARGOS_1004 c...  56.5    2e-05
CP042383.1 Leuconostoc pseudomesenteroides strain CBA3630 chromos...  73.1    4e-10
CP070515.1 Leuconostoc falkenbergense strain FDAARGOS_1201 chromo...  73.1    4e-10
CP065993.1 Leuconostoc pseudomesenteroides strain FDAARGOS_1003 c...  73.1    4e-10
CP065975.1 Leuconostoc pseudomesenteroides strain FDAARGOS_1004 c...  73.1    4e-10
CP028255.1 Leuconostoc mesenteroides strain SRCM102735 chromosome...  71.3    2e-09
CP028251.1 Leuconostoc mesenteroides strain SRCM102733 chromosome...  71.3    2e-09
CP046062.1 Leuconostoc mesenteroides subsp. mesenteroides strain ...  71.3    2e-09
CP042404.1 Leuconostoc mesenteroides strain CBA3628 chromosome, c...  71.3    2e-09
CP035746.1 Leuconostoc mesenteroides strain SRCM103460 chromosome...  71.3    2e-09
CP035271.1 Leuconostoc mesenteroides strain SRCM103453 chromosome...  71.3    2e-09
CP035139.1 Leuconostoc mesenteroides strain SRCM103356 chromosome...  71.3    2e-09
CP021966.1 Leuconostoc mesenteroides strain CBA7131 chromosome, c...  71.3    2e-09
CP021491.1 Leuconostoc mesenteroides strain WiKim33 chromosome, c...  71.3    2e-09
AP017936.1 Leuconostoc mesenteroides DNA, complete genome, strain...  71.3    2e-09
AP017935.1 Leuconostoc mesenteroides subsp. suionicum DNA, comple...  71.3    2e-09
CP020731.1 Leuconostoc mesenteroides subsp. mesenteroides FM06, c...  71.3    2e-09
CP015442.1 Leuconostoc mesenteroides subsp. mesenteroides strain ...  71.3    2e-09
CP013016.1 Leuconostoc mesenteroides subsp. mesenteroides strain ...  71.3    2e-09
CP015247.1 Leuconostoc mesenteroides subsp. suionicum strain DSM ...  71.3    2e-09
CP014611.1 Leuconostoc mesenteroides subsp. jonggajibkimchii stra...  71.3    2e-09
CP014610.1 Leuconostoc mesenteroides subsp. mesenteroides strain ...  71.3    2e-09
CP003101.3 Leuconostoc mesenteroides subsp. mesenteroides J18, co...  71.3    2e-09
CP012009.1 Leuconostoc mesenteroides subsp. dextranicum strain DS...  71.3    2e-09
CP000574.1 Leuconostoc mesenteroides KFRI-MG, complete genome         71.3    2e-09
CP037752.1 Leuconostoc mesenteroides strain WiKim32 chromosome, c...  71.3    2e-09
CP048006.1 Leuconostoc mesenteroides strain wikim19 chromosome, c...  71.3    2e-09
CP065995.1 Leuconostoc mesenteroides strain FDAARGOS_1033 chromos...  71.3    2e-09
CP000414.1 Leuconostoc mesenteroides subsp. mesenteroides ATCC 82...  71.3    2e-09
LK009910.1 TPA: Leuconostoc mesenteroides subsp. mesenteroides AT...  69.4    6e-09
CP042383.1 Leuconostoc pseudomesenteroides strain CBA3630 chromos...  95.3    2e-16
CP070515.1 Leuconostoc falkenbergense strain FDAARGOS_1201 chromo...  95.3    2e-16
CP065993.1 Leuconostoc pseudomesenteroides strain FDAARGOS_1003 c...  95.3    2e-16
CP065975.1 Leuconostoc pseudomesenteroides strain FDAARGOS_1004 c...  95.3    2e-16
CP028255.1 Leuconostoc mesenteroides strain SRCM102735 chromosome...  87.9    4e-14
CP028251.1 Leuconostoc mesenteroides strain SRCM102733 chromosome...  87.9    4e-14
CP046149.1 Leuconostoc citreum strain WiKim0101 chromosome            87.9    4e-14
CP046062.1 Leuconostoc mesenteroides subsp. mesenteroides strain ...  87.9    4e-14
CP042418.1 Leuconostoc citreum strain CBA3627 chromosome, complet...  87.9    4e-14
CP042420.1 Leuconostoc lactis strain CBA3622 chromosome, complete...  87.9    4e-14
CP042410.1 Leuconostoc citreum strain CBA3621 chromosome, complet...  87.9    4e-14
CP042393.1 Leuconostoc citreum strain CBA3623 chromosome, complet...  87.9    4e-14
CP042390.1 Leuconostoc lactis strain CBA3626 chromosome, complete...  87.9    4e-14
CP042404.1 Leuconostoc mesenteroides strain CBA3628 chromosome, c...  87.9    4e-14
CP042387.1 Leuconostoc lactis strain CBA3625 chromosome, complete...  87.9    4e-14
CP042413.1 Leuconostoc citreum strain CBA3624 chromosome, complet...  87.9    4e-14
CP017196.1 Leuconostoc gelidum subsp. gelidum strain TMW 2.1618, ...  87.9    4e-14
CP035746.1 Leuconostoc mesenteroides strain SRCM103460 chromosome...  87.9    4e-14
CP035271.1 Leuconostoc mesenteroides strain SRCM103453 chromosome...  87.9    4e-14
CP035139.1 Leuconostoc mesenteroides strain SRCM103356 chromosome...  87.9    4e-14
CP021966.1 Leuconostoc mesenteroides strain CBA7131 chromosome, c...  87.9    4e-14
CP024929.1 Leuconostoc citreum strain EFEL 2700 chromosome, compl...  87.9    4e-14
CP021491.1 Leuconostoc mesenteroides strain WiKim33 chromosome, c...  87.9    4e-14
AP017936.1 Leuconostoc mesenteroides DNA, complete genome, strain...  87.9    4e-14
AP017935.1 Leuconostoc mesenteroides subsp. suionicum DNA, comple...  87.9    4e-14
CP020731.1 Leuconostoc mesenteroides subsp. mesenteroides FM06, c...  87.9    4e-14
CP015442.1 Leuconostoc mesenteroides subsp. mesenteroides strain ...  87.9    4e-14
CP013016.1 Leuconostoc mesenteroides subsp. mesenteroides strain ...  87.9    4e-14
CP016329.1 Leuconostoc garlicum strain KFRI01, complete genome        87.9    4e-14
CP015247.1 Leuconostoc mesenteroides subsp. suionicum strain DSM ...  87.9    4e-14
CP014611.1 Leuconostoc mesenteroides subsp. jonggajibkimchii stra...  87.9    4e-14
CP016598.1 Leuconostoc lactis strain WiKim40, complete sequence       87.9    4e-14
CP014610.1 Leuconostoc mesenteroides subsp. mesenteroides strain ...  87.9    4e-14
CP003101.3 Leuconostoc mesenteroides subsp. mesenteroides J18, co...  87.9    4e-14
CP012009.1 Leuconostoc mesenteroides subsp. dextranicum strain DS...  87.9    4e-14
CP000574.1 Leuconostoc mesenteroides KFRI-MG, complete genome         87.9    4e-14
CP037752.1 Leuconostoc mesenteroides strain WiKim32 chromosome, c...  87.9    4e-14
CP048006.1 Leuconostoc mesenteroides strain wikim19 chromosome, c...  87.9    4e-14
CP003839.1 Leuconostoc gelidum JB7, complete genome                   87.9    4e-14
CP066296.1 Leuconostoc citreum strain WiKim0096 chromosome            87.9    4e-14
CP065995.1 Leuconostoc mesenteroides strain FDAARGOS_1033 chromos...  87.9    4e-14
CP063830.1 Leuconostoc citreum strain 37 chromosome, complete genome  87.9    4e-14
CP039736.1 Leuconostoc sp. LN180020 chromosome, complete genome       87.9    4e-14
DQ489736.1 Leuconostoc citreum KM20, complete genome                  87.9    4e-14
CP000414.1 Leuconostoc mesenteroides subsp. mesenteroides ATCC 82...  87.9    4e-14
LK009790.1 TPA: Leuconostoc citreum KM20 tRNA-Asn-GTT-1-1 gene        86.1    1e-13
CP021966.1 Leuconostoc mesenteroides strain CBA7131 chromosome, c...  56.5    2e-05
CP015442.1 Leuconostoc mesenteroides subsp. mesenteroides strain ...  56.5    2e-05
CP014610.1 Leuconostoc mesenteroides subsp. mesenteroides strain ...  52.8    2e-04
CP065975.1 Leuconostoc pseudomesenteroides strain FDAARGOS_1004 c...  71.3    2e-09
CP021966.1 Leuconostoc mesenteroides strain CBA7131 chromosome, c...  56.5    2e-05
CP015442.1 Leuconostoc mesenteroides subsp. mesenteroides strain ...  56.5    2e-05
CP014610.1 Leuconostoc mesenteroides subsp. mesenteroides strain ...  52.8    2e-04
CP065975.1 Leuconostoc pseudomesenteroides strain FDAARGOS_1004 c...  56.5    2e-05
CP065975.1 Leuconostoc pseudomesenteroides strain FDAARGOS_1004 c...  84.2    5e-13
CP042383.1 Leuconostoc pseudomesenteroides strain CBA3630 chromos...  78.7    2e-11
CP070515.1 Leuconostoc falkenbergense strain FDAARGOS_1201 chromo...  78.7    2e-11
CP065993.1 Leuconostoc pseudomesenteroides strain FDAARGOS_1003 c...  78.7    2e-11
AP017935.1 Leuconostoc mesenteroides subsp. suionicum DNA, comple...  102     2e-18
CP015442.1 Leuconostoc mesenteroides subsp. mesenteroides strain ...  102     2e-18
CP015247.1 Leuconostoc mesenteroides subsp. suionicum strain DSM ...  102     2e-18
CP014610.1 Leuconostoc mesenteroides subsp. mesenteroides strain ...  102     2e-18
DQ489736.1 Leuconostoc citreum KM20, complete genome                  102     2e-18
CP042393.1 Leuconostoc citreum strain CBA3623 chromosome, complet...  97.1    7e-17
AP017936.1 Leuconostoc mesenteroides DNA, complete genome, strain...  97.1    7e-17
CP020731.1 Leuconostoc mesenteroides subsp. mesenteroides FM06, c...  56.5    2e-05
BK021896.1 TPA: Siphoviridae sp. isolate ct6uq2, partial genome       56.5    2e-05
CP070515.1 Leuconostoc falkenbergense strain FDAARGOS_1201 chromo...  56.5    2e-05
CP065975.1 Leuconostoc pseudomesenteroides strain FDAARGOS_1004 c...  56.5    2e-05
CP017197.1 Leuconostoc gelidum subsp. gasicomitatum strain TMW 2....  54.7    6e-05
CP063830.1 Leuconostoc citreum strain 37 chromosome, complete genome  54.7    6e-05
MN855637.1 Bacteriophage sp. isolate 32, complete genome              56.5    2e-05
CP042383.1 Leuconostoc pseudomesenteroides strain CBA3630 chromos...  56.5    2e-05
CP021491.1 Leuconostoc mesenteroides strain WiKim33 chromosome, c...  56.5    2e-05
CP065993.1 Leuconostoc pseudomesenteroides strain FDAARGOS_1003 c...  56.5    2e-05
NC_055037.1 Leuconostoc phage phiMH1, partial genome                  56.5    2e-05
CP053337.1 Lactiplantibacillus paraplantarum strain CK401 chromos...  52.8    0.001
CP050919.1 Limosilactobacillus fermentum strain HFD1 chromosome, ...  52.8    0.001
CP047584.1 Limosilactobacillus fermentum strain AGR1485 chromosom...  52.8    0.001
CP048921.1 Lactiplantibacillus plantarum strain X7022 chromosome,...  52.8    0.001
CP048116.1 Latilactobacillus sakei strain MBEL1397 chromosome, co...  52.8    0.001
CP046669.1 Lactiplantibacillus plantarum strain 83-18 chromosome,...  52.8    0.001
CP046661.1 Lactiplantibacillus plantarum strain 83-18 plasmid p83...  52.8    0.001
CP046263.1 Lactiplantibacillus plantarum strain KCCP11226 plasmid...  52.8    0.001
CP045034.1 Limosilactobacillus fermentum strain USM 8633 chromoso...  52.8    0.001
CP046037.1 Latilactobacillus sakei strain CBA3614 chromosome, com...  52.8    0.001
CP045037.1 Liquorilactobacillus mali strain LM596 plasmid unnamed...  52.8    0.001
CP045035.1 Liquorilactobacillus mali strain LM596 chromosome, com...  52.8    0.001
CP044354.1 Limosilactobacillus fermentum strain 2760 chromosome, ...  52.8    0.001
CP042399.1 Weissella hellenica strain CBA3632 chromosome, complet...  52.8    0.001
CP042374.1 Leuconostoc carnosum strain CBA3620 chromosome, comple...  52.8    0.001
CP033608.1 Weissella hellenica strain 0916-4-2 chromosome, comple...  52.8    0.001
CP031209.1 Levilactobacillus brevis strain UCCLB521 plasmid pUCCL...  52.8    0.001
CP031178.1 Levilactobacillus brevis strain UCCLB556 plasmid pUCCL...  52.8    0.001
CP039750.1 Limosilactobacillus fermentum strain B1 28 chromosome      52.8    0.001
CP033618.1 Lactiplantibacillus plantarum strain J26 plasmid pJ26p...  52.8    0.001
CP033617.1 Lactiplantibacillus plantarum strain J26 plasmid pJ26p...  52.8    0.001
CP033616.1 Lactiplantibacillus plantarum strain J26 chromosome, c...  52.8    0.001
CP026507.1 Lactiplantibacillus plantarum strain NCIMB700965.EF.A ...  52.8    0.001
CP026505.1 Lactiplantibacillus plantarum strain NCIMB700965.EF.A ...  52.8    0.001
CP034099.1 Limosilactobacillus fermentum strain LMT2-75 chromosom...  52.8    0.001
CP033371.1 Limosilactobacillus fermentum strain DR9 chromosome, c...  52.8    0.001
CP032753.1 Lactiplantibacillus argentoratensis strain DSM 16365 p...  52.8    0.001
CP032746.1 Lactiplantibacillus paraplantarum strain DSM 10667 pla...  52.8    0.001
CP032653.1 Latilactobacillus sakei strain LZ217 plasmid unnamed1,...  52.8    0.001
CP032652.1 Latilactobacillus sakei strain LZ217 chromosome, compl...  52.8    0.001
CP032645.1 Lactiplantibacillus plantarum strain ZFM9 plasmid unna...  52.8    0.001
CP032634.1 Latilactobacillus sakei strain ZFM220 plasmid unnamed1...  52.8    0.001
CP032633.1 Latilactobacillus sakei strain ZFM220 chromosome, comp...  52.8    0.001
CP032636.1 Latilactobacillus sakei strain ZFM225 plasmid unnamed1...  52.8    0.001
CP032635.1 Latilactobacillus sakei strain ZFM225 chromosome, comp...  52.8    0.001
CP032641.1 Latilactobacillus sakei strain ZFM229 plasmid unnamed1...  52.8    0.001
CP032640.1 Latilactobacillus sakei strain ZFM229 chromosome, comp...  52.8    0.001
CP023492.1 Lactiplantibacillus plantarum strain NCIMB 700965 plas...  52.8    0.001
CP023490.1 Lactiplantibacillus plantarum strain NCIMB 700965 chro...  52.8    0.001
CP031004.1 Latilactobacillus curvatus strain TMW 1.1928 plasmid p...  52.8    0.001
CP031003.1 Latilactobacillus curvatus strain TMW 1.1928 chromosom...  52.8    0.001
CP035567.1 Lactiplantibacillus plantarum strain SRCM103300 plasmi...  52.8    0.001
MK185231.1 Lactobacillus sakei strain TMW 1.2101 CRISPR repeat re...  52.8    0.001
CP035153.1 Pediococcus acidilactici strain SRCM103367 plasmid unn...  52.8    0.001
CP026116.1 Latilactobacillus curvatus JCM 1096 = DSM 20019 chromo...  52.8    0.001
CP035176.1 Lactiplantibacillus plantarum strain SRCM103426 plasmi...  52.8    0.001
CP035115.1 Lactiplantibacillus plantarum strain SRCM103295 plasmi...  52.8    0.001
CP035114.1 Lactiplantibacillus plantarum strain SRCM103295 plasmi...  52.8    0.001
CP035054.1 Limosilactobacillus fermentum strain SRCM103285 chromo...  52.8    0.001
CP035028.1 Lactiplantibacillus plantarum strain YW11 plasmid plsm...  52.8    0.001
CP035023.1 Lactiplantibacillus plantarum strain 13_3 plasmid plsm...  52.8    0.001
CP018181.1 Liquorilactobacillus nagelii strain TMW 1.1827 plasmid...  52.8    0.001
CP018178.1 Liquorilactobacillus hordei strain TMW 1.1822 plasmid ...  52.8    0.001
CP018177.1 Liquorilactobacillus hordei strain TMW 1.1822 plasmid ...  52.8    0.001
CP025476.1 Latilactobacillus curvatus strain IRG2 chromosome, com...  52.8    0.001
CP031195.1 Limosilactobacillus fermentum strain LDTM 7301 chromos...  52.8    0.001
CP025206.1 Latilactobacillus sakei strain WiKim0074 chromosome, c...  52.8    0.001
CP025204.1 Latilactobacillus sakei strain WiKim0073 plasmid pLSW7...  52.8    0.001
CP025203.1 Latilactobacillus sakei strain WiKim0073 chromosome        52.8    0.001
CP025137.1 Latilactobacillus sakei strain WiKim0072 plasmid pLSW7...  52.8    0.001
CP025136.1 Latilactobacillus sakei strain WiKim0072 chromosome, c...  52.8    0.001
CP029966.1 Latilactobacillus curvatus strain ZJUNIT8 chromosome, ...  52.8    0.001
CP021964.1 Limosilactobacillus fermentum strain CBA7106 chromosom...  52.8    0.001
AP018699.1 Lactobacillus curvatus NFH-Km12 DNA, complete genome       52.8    0.001
CP028424.1 Lactiplantibacillus plantarum strain AS-6 chromosome       52.8    0.001
CP028420.1 Lactiplantibacillus plantarum strain AS-10 chromosome      52.8    0.001
CP020097.1 Lactiplantibacillus plantarum strain K25 plasmid unnam...  52.8    0.001
CP019982.1 Pediococcus inopinatus strain DSM 20285 plasmid pLDW-1...  52.8    0.001
CP027191.1 Lactobacillus sp. CBA3605 plasmid pCBA3605-1, complete...  52.8    0.001
CP027190.1 Lactobacillus sp. CBA3605 chromosome, complete genome      52.8    0.001
LT906621.1 Lactobacillus fermentum strain IMDO 130101 genome asse...  52.8    0.001
CP025415.1 Lactiplantibacillus plantarum strain X7021 plasmid unn...  52.8    0.001
CP023501.1 Weissella paramesenteroides strain FDAARGOS_414 chromo...  52.8    0.001
CP016803.1 Lactobacillus fermentum strain SK152, complete genome      52.8    0.001
CP022710.1 Latilactobacillus sakei strain WiKim0063 plasmid pLBS0...  52.8    0.001
CP022709.1 Latilactobacillus sakei strain WiKim0063 chromosome, c...  52.8    0.001
CP022475.1 Latilactobacillus curvatus strain KG6 chromosome, comp...  52.8    0.001
CP020459.1 Latilactobacillus sakei strain FAM18311, partial sequence  52.8    0.001
CP021790.1 Limosilactobacillus fermentum strain LAC FRN-92 chromo...  52.8    0.001
CP021530.1 Pediococcus acidilactici strain SRCM101189 plasmid pPL...  52.8    0.001
CP021488.1 Pediococcus acidilactici strain SRCM100313 plasmid pPl...  52.8    0.001
CP021485.1 Pediococcus acidilactici strain SRCM100424 plasmid pPl...  52.8    0.001
AP017929.1 Lactobacillus sakei subsp. sakei DSM 20017 = JCM 1157 ...  52.8    0.001
CP020604.1 Lactococcus lactis subsp. lactis bv. diacetylactis str...  52.8    0.001
CP021104.1 Limosilactobacillus fermentum strain FTDC 8312 chromos...  52.8    0.001
CP019751.1 Levilactobacillus brevis strain TMW 1.2113 plasmid pl1...  52.8    0.001
CP016802.1 Lactobacillus brevis strain TMW 1.2112 plasmid pl12112...  52.8    0.001
CP018212.1 Lactobacillus plantarum strain BLS41 plasmid pLPBLS41_...  52.8    0.001
CP018211.1 Lactobacillus plantarum strain BLS41 plasmid pLPBLS41_...  52.8    0.001
CP020353.1 Limosilactobacillus fermentum strain VRI-003 chromosome    52.8    0.001
CP019030.1 Limosilactobacillus fermentum strain SNUV175 chromosom...  52.8    0.001
CP014333.1 Weissella jogaejeotgali strain FOL01 plasmid pFOL01, c...  52.8    0.001
CP014332.1 Weissella jogaejeotgali strain FOL01, complete sequence    52.8    0.001
CP018326.1 Lactiplantibacillus plantarum subsp. plantarum strain ...  52.8    0.001
CP017712.1 Lactobacillus fermentum strain 47-7 genome                 52.8    0.001
CP017151.1 Lactobacillus fermentum strain NCC2970, complete genome    52.8    0.001
CP014907.1 Fructilactobacillus lindneri strain TMW 1.481 chromoso...  52.8    0.001
CP014872.1 Fructilactobacillus lindneri strain TMW 1.1993 chromos...  52.8    0.001
CP016602.1 Latilactobacillus curvatus strain WiKim52, complete se...  52.8    0.001
CP014629.1 Lactobacillus backii strain TMW 1.1988 plasmid L11988-...  52.8    0.001
CP016028.1 Lactobacillus curvatus strain FBA2, complete genome        52.8    0.001
CP012283.1 Pediococcus damnosus strain TMW 2.1534, complete genome    52.8    0.001
CP012269.1 Pediococcus damnosus strain TMW 2.1532, complete genome    52.8    0.001
KT149389.1 Lactobacillus plantarum strain BM4 plasmid pBM3, compl...  52.8    0.001
CP012660.1 Lactobacillus plantarum strain HFC8 plasmid pMK06, com...  52.8    0.001
CP012657.1 Lactobacillus plantarum strain HFC8 plasmid pMK01, com...  52.8    0.001
CP011536.1 Lactobacillus fermentum 3872, complete genome              52.8    0.001
CP005943.2 Lactobacillus plantarum subsp. plantarum P-8 plasmid L...  52.8    0.001
CP082359.1 Limosilactobacillus fermentum strain ACA-DC 179 chromo...  52.8    0.001
CP076832.1 Lactiplantibacillus plantarum 2025 plasmid LP2025_8, c...  52.8    0.001
CP076829.1 Lactiplantibacillus plantarum 2025 plasmid LP2025_5, c...  52.8    0.001
MW265923.1 Latilactobacillus sakei strain Yak-LI plasmid unnamed2...  52.8    0.001
CP065522.1 Limosilactobacillus fermentum strain YLF016 chromosome...  52.8    0.001
CP076446.1 Limosilactobacillus fermentum strain L1 chromosome, co...  52.8    0.001
AP024685.1 Latilactobacillus curvatus WDN19 DNA, complete genome      52.8    0.001
CP076082.1 Limosilactobacillus fermentum strain 9-4 chromosome, c...  52.8    0.001
CP006033.1 Lactobacillus plantarum 16, complete genome                52.8    0.001
CP005958.1 Lactobacillus fermentum F-6, complete genome               52.8    0.001
CP071897.1 Lactiplantibacillus plantarum strain KLDS1.0386 plasmi...  52.8    0.001
CP071896.1 Lactiplantibacillus plantarum strain KLDS1.0386 plasmi...  52.8    0.001
CP071895.1 Lactiplantibacillus plantarum strain KLDS1.0386 plasmi...  52.8    0.001
CP071450.1 Lactiplantibacillus plantarum strain AR195 plasmid unn...  52.8    0.001
CP071001.1 Limosilactobacillus fermentum strain GR1007 chromosome...  52.8    0.001
CP071000.1 Limosilactobacillus fermentum strain GR1008 chromosome...  52.8    0.001
CP070999.1 Limosilactobacillus fermentum strain GR1009 chromosome...  52.8    0.001
CP070858.1 Limosilactobacillus fermentum strain GR1103 chromosome...  52.8    0.001
CP069287.1 Lactiplantibacillus plantarum strain KM2 plasmid pKM2-...  52.8    0.001
CP069285.1 Lactiplantibacillus plantarum strain KM2 plasmid pKM2-...  52.8    0.001
CP068768.1 Lactiplantibacillus plantarum strain S58 plasmid p3, c...  52.8    0.001
CP068767.1 Lactiplantibacillus plantarum strain S58 chromosome, c...  52.8    0.001
AP024320.1 Limosilactobacillus fermentum ike38 DNA, complete genome   52.8    0.001
CP067088.1 Limosilactobacillus fermentum strain B44 chromosome, c...  52.8    0.001
CP066551.1 Lactiplantibacillus plantarum strain SHY 21-2 plasmid ...  52.8    0.001
CP066550.1 Lactiplantibacillus plantarum strain SHY 21-2 plasmid ...  52.8    0.001
CP065046.1 Weissella paramesenteroides strain STCH-BD1 chromosome     52.8    0.001
CP064817.1 Latilactobacillus sakei strain ye2 chromosome, complet...  52.8    0.001
CP061326.1 Lactococcus lactis subsp. lactis bv. diacetylactis str...  52.8    0.001
CP059698.1 Latilactobacillus sakei strain CBA3635 plasmid pCBA363...  52.8    0.001
CP059697.1 Latilactobacillus sakei strain CBA3635 chromosome, com...  52.8    0.001
CP040910.1 Limosilactobacillus fermentum strain DSM 20052 chromosome  52.8    0.001
CP002033.1 Lactobacillus fermentum CECT 5716, complete genome         52.8    0.001
AP008937.1 Lactobacillus fermentum IFO 3956 DNA, complete genome      52.8    0.001
AJ626710.1 Lactobacillus sakei sakacin P related gene cluster, st...  52.8    0.001
Z46867.1 L.sake sakacin A gene cluster                                52.8    0.001
X75164.1 L.sake insertion sequence IS1163 (complete)                  52.8    0.001
CP065975.1 Leuconostoc pseudomesenteroides strain FDAARGOS_1004 c...  56.5    2e-05
CP065975.1 Leuconostoc pseudomesenteroides strain FDAARGOS_1004 c...  56.5    2e-05
CP065975.1 Leuconostoc pseudomesenteroides strain FDAARGOS_1004 c...  58.4    5e-06
CP017197.1 Leuconostoc gelidum subsp. gasicomitatum strain TMW 2....  56.5    2e-05
CP063830.1 Leuconostoc citreum strain 37 chromosome, complete genome  56.5    2e-05
BK021896.1 TPA: Siphoviridae sp. isolate ct6uq2, partial genome       52.8    2e-04
CP035271.1 Leuconostoc mesenteroides strain SRCM103453 chromosome...  132     6e-27
CP021966.1 Leuconostoc mesenteroides strain CBA7131 chromosome, c...  132     6e-27
