#!/usr/bin/env python3

import argparse

parser = argparse.ArgumentParser(description="Infiles")

parser.add_argument('-i', '--infile', help=("Input files"), nargs='+', metavar='In.fasta', default=None)
args = parser.parse_args()

infile = open(args.infile[0],'r')

pairs = list()

for line in infile:
    if line.startswith('['):
        line = line.split('\'')
        pairs.append([int(line[1]),int(line[3])])
        pairs.sort()

infile.close()

outfile = open('OtherPosSort.txt','w')

for pair in pairs:
    print(pair,file=outfile)

outfile.close()
