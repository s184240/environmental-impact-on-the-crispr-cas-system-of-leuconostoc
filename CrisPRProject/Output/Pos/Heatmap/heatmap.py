#!/usr/bin/env python3

import seaborn as sns
import xlrd
import numpy as np




#Path to data
doc = xlrd.open_workbook('C:/Users/Mikkel/Downloads/DairyPos.xls').sheet_by_index(0)

#Generate Matrix
ATnames = doc.row_values(rowx=0, start_colx=0, end_colx=3)

classLabels = doc.col_values(0,1,None)
classNames = sorted(set(classLabels))
classDict = dict(zip(classNames,range(len(classNames))))

y = np.array([classDict[value] for value in classLabels])

X = np.empty((len(classNames)-1,2))
for i in range(2):
    X[:,i] = np.array(doc.col_values(i+1,1,len(classNames))).T


#Make heatmap

Heatmap=sns.heatmap(X,vmin=1, vmax=900000, xticklabels=False, cmap="Reds_r",)


