#usr/bin/env python3

import sys
import os
import argparse

def readspacer(Input,Output,ID):
    infile=open(Input,'r')
    ID = ID.split('/')[-2]
    ID = ID[7:-19]
    print('>'+ID, file=Output)
    flag = False
    pospair=list()
    for line in infile:
        if line.startswith('\"Name\"'):
           flag = True
        if flag == True and line.startswith('\"Start\"'):
           line = line.split(' ')[1]
           line = line[:-2]
           pospair.append(line)
        if flag == True and line.startswith('\"End\"'):
            line = line.split(' ')[1]
            line = line[:-2]
            pospair.append(line)
            print(pospair, file=Output)
            pospair.clear()
            flag = False
    infile.close()
    return(None)

outfile=open('OtherPos.txt', 'w')

parser = argparse.ArgumentParser(description="Infiles")

parser.add_argument('-i', '--infile', help=("Input files"), nargs='+', metavar='In.fasta', default=None)
args = parser.parse_args()

for i in range(len(args.infile)):
    readspacer(args.infile[i],outfile,args.infile[i])
outfile.close()
