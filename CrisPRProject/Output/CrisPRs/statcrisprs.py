#usr/bin/env python3

import argparse
import sys
import re

parser = argparse.ArgumentParser(description="Infiles")

parser.add_argument('-i', '--infile', help=("Input files"), nargs='+', metavar='In.fasta', default=None)
args = parser.parse_args()

infile = open(args.infile[0],'r')

crisprs=list()

samples=0

for line in infile:
    if line.startswith('>'):
       samples += 1
    elif re.search('\d',line) != None:
       pass
    else:
       crisprs.append(line[:-1])

infile.close()

avg = 0

for repeat in crisprs:
    avg += len(repeat)

avg = avg/len(crisprs)

lenghts = list()

for repeat in crisprs:
    lenghts.append(len(repeat))


print('Average repeats found: '+str(len(crisprs)/samples))
print('Average repeat length: '+ str(avg))
print(lenghts)
