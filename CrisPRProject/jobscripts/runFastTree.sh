#!/bin/sh

#PBS -A cge -W group_list=cge
#PBS -l nodes=1:ppn=1
#PBS -l mem=2GB walltime=0:30:00 
#PBS -N mikwoljob1
#PBS -o /dev/null
module purge

module load tools
module load fasttree/2.1.11

cd /home/projects/cge/people/rkmo/temp/mikkel/CrisPRProject/Output/Phylogeny/CorePhyl/

/services/tools/fasttree/2.1.11/FastTree -nt < core_gene_alignment.aln > CoreTree.tree
