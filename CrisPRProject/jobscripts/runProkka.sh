#!/bin/sh
#PBS -A cge -W group_list=cge
#PBS -o /dev/null -e /dev/null
#PBS -N mikwoljob
 
module purge
module load tools
module load perl/5.30.2
module load clustalw/2.1
module load prodigal/2.6.3
module load hmmer/3.2.1
module load ncbi-blast/2.10.0+
module load aragorn/1.2.36
module load tbl2asn/20200706
module load infernal/1.1.2
module load barrnap/0.7
module load prokka/1.14.5
 
cd /home/projects/cge/people/rkmo/temp/mikkel/CrisPRProject/Output/Phylogeny/Core
prokka --outdir GeneAnnotation0 --prefix Annot0 /home/projects/cge/scratch/people/mikwol/data/Leuconostoc_Data/Dairy/GCA_000686465.1_Leuconostoc_genomic.fna
prokka --outdir GeneAnnotation1 --prefix Annot1 /home/projects/cge/scratch/people/mikwol/data/Leuconostoc_Data/Dairy/GCA_000686505.1_Leuconostoc_genomic.fna
prokka --outdir GeneAnnotation2 --prefix Annot2 /home/projects/cge/scratch/people/mikwol/data/Leuconostoc_Data/Dairy/GCA_002072475.1_ASM207247v1_genomic.fna
prokka --outdir GeneAnnotation3 --prefix Annot3 /home/projects/cge/scratch/people/mikwol/data/Leuconostoc_Data/Dairy/GCA_002072495.1_ASM207249v1_genomic.fna
prokka --outdir GeneAnnotation4 --prefix Annot4 /home/projects/cge/scratch/people/mikwol/data/Leuconostoc_Data/Dairy/GCA_002072505.1_ASM207250v1_genomic.fna
prokka --outdir GeneAnnotation5 --prefix Annot5 /home/projects/cge/scratch/people/mikwol/data/Leuconostoc_Data/Dairy/GCA_002072555.1_ASM207255v1_genomic.fna
prokka --outdir GeneAnnotation6 --prefix Annot6 /home/projects/cge/scratch/people/mikwol/data/Leuconostoc_Data/Dairy/GCA_002072565.1_ASM207256v1_genomic.fna
prokka --outdir GeneAnnotation7 --prefix Annot7 /home/projects/cge/scratch/people/mikwol/data/Leuconostoc_Data/Dairy/GCA_002072575.1_ASM207257v1_genomic.fna
prokka --outdir GeneAnnotation8 --prefix Annot8 /home/projects/cge/scratch/people/mikwol/data/Leuconostoc_Data/Dairy/GCA_002072585.1_ASM207258v1_genomic.fna
prokka --outdir GeneAnnotation9 --prefix Annot9 /home/projects/cge/scratch/people/mikwol/data/Leuconostoc_Data/Dairy/GCA_002092035.1_ASM209203v1_genomic.fna
prokka --outdir GeneAnnotation10 --prefix Annot10 /home/projects/cge/scratch/people/mikwol/data/Leuconostoc_Data/Dairy/GCA_002092075.1_ASM209207v1_genomic.fna
prokka --outdir GeneAnnotation11 --prefix Annot11 /home/projects/cge/scratch/people/mikwol/data/Leuconostoc_Data/Dairy/GCA_002092165.1_ASM209216v1_genomic.fna
prokka --outdir GeneAnnotation12 --prefix Annot12 /home/projects/cge/scratch/people/mikwol/data/Leuconostoc_Data/Dairy/GCA_002092235.1_ASM209223v1_genomic.fna
prokka --outdir GeneAnnotation13 --prefix Annot13 /home/projects/cge/scratch/people/mikwol/data/Leuconostoc_Data/Dairy/GCA_002092255.1_ASM209225v1_genomic.fna
prokka --outdir GeneAnnotation14 --prefix Annot14 /home/projects/cge/scratch/people/mikwol/data/Leuconostoc_Data/Dairy/GCA_002092295.1_ASM209229v1_genomic.fna
prokka --outdir GeneAnnotation15 --prefix Annot15 /home/projects/cge/scratch/people/mikwol/data/Leuconostoc_Data/Dairy/GCA_002092355.1_ASM209235v1_genomic.fna
prokka --outdir GeneAnnotation16 --prefix Annot16 /home/projects/cge/scratch/people/mikwol/data/Leuconostoc_Data/Dairy/GCA_002092375.1_ASM209237v1_genomic.fna
prokka --outdir GeneAnnotation17 --prefix Annot17 /home/projects/cge/scratch/people/mikwol/data/Leuconostoc_Data/Dairy/GCA_002092535.1_ASM209253v1_genomic.fna
prokka --outdir GeneAnnotation18 --prefix Annot18 /home/projects/cge/scratch/people/mikwol/data/Leuconostoc_Data/Dairy/GCA_002092555.1_ASM209255v1_genomic.fna
prokka --outdir GeneAnnotation19 --prefix Annot19 /home/projects/cge/scratch/people/mikwol/data/Leuconostoc_Data/Dairy/GCA_002092635.1_ASM209263v1_genomic.fna
prokka --outdir GeneAnnotation20 --prefix Annot20 /home/projects/cge/scratch/people/mikwol/data/Leuconostoc_Data/Dairy/GCA_002092645.1_ASM209264v1_genomic.fna
prokka --outdir GeneAnnotation21 --prefix Annot21 /home/projects/cge/scratch/people/mikwol/data/Leuconostoc_Data/Dairy/GCA_003346375.1_ASM334637v1_genomic.fna
prokka --outdir GeneAnnotation22 --prefix Annot22 /home/projects/cge/scratch/people/mikwol/data/Leuconostoc_Data/Plants/GCA_008033175.1_ASM803317v1_genomic.fna
prokka --outdir GeneAnnotation23 --prefix Annot23 /home/projects/cge/scratch/people/mikwol/data/Leuconostoc_Data/Plants/GCA_012396745.1_ASM1239674v1_genomic.fna
prokka --outdir GeneAnnotation24 --prefix Annot24 /home/projects/cge/scratch/people/mikwol/data/Leuconostoc_Data/Plants/GCA_018257095.1_ASM1825709v1_genomic.fna
prokka --outdir GeneAnnotation25 --prefix Annot25 /home/projects/cge/scratch/people/mikwol/data/Leuconostoc_Data/Other/GCA_006382035.1_ASM638203v1_genomic.fna
prokka --outdir GeneAnnotation26 --prefix Annot26 /home/projects/cge/scratch/people/mikwol/data/Leuconostoc_Data/Other/GCA_901830415.1_AMBR10_genomic.fna
prokka --outdir GeneAnnotation27 --prefix Annot27 /home/projects/cge/scratch/people/mikwol/data/Assemblies/DK0099_R1.fa
prokka --outdir GeneAnnotation28 --prefix Annot28 /home/projects/cge/scratch/people/mikwol/data/Assemblies/DK0180_R1.fa
prokka --outdir GeneAnnotation29 --prefix Annot29 /home/projects/cge/scratch/people/mikwol/data/Assemblies/DK0181_R1.fa
prokka --outdir GeneAnnotation30 --prefix Annot30 /home/projects/cge/scratch/people/mikwol/data/Assemblies/DK0182_R1.fa
prokka --outdir GeneAnnotation31 --prefix Annot31 /home/projects/cge/scratch/people/mikwol/data/Assemblies/DK0227_R1.fa
prokka --outdir GeneAnnotation32 --prefix Annot32 /home/projects/cge/scratch/people/mikwol/data/Assemblies/DK0228_R1.fa
prokka --outdir GeneAnnotation33 --prefix Annot33 /home/projects/cge/scratch/people/mikwol/data/Assemblies/DK0313_R1.fa
prokka --outdir GeneAnnotation34 --prefix Annot34 /home/projects/cge/scratch/people/mikwol/data/Assemblies/DK0314_R1.fa
prokka --outdir GeneAnnotation35 --prefix Annot35 /home/projects/cge/scratch/people/mikwol/data/Assemblies/DK0322_R1.fa
prokka --outdir GeneAnnotation36 --prefix Annot36 /home/projects/cge/scratch/people/mikwol/data/Assemblies/DK0323_R1.fa
prokka --outdir GeneAnnotation37 --prefix Annot37 /home/projects/cge/scratch/people/mikwol/data/Assemblies/DK0339_R1.fa
prokka --outdir GeneAnnotation38 --prefix Annot38 /home/projects/cge/scratch/people/mikwol/data/Assemblies/DTU2020-371-PRJ1246-Leuconostoc-pseudomesenteroides-DK96_S8_L001_R1_001.fa
prokka --outdir GeneAnnotation39 --prefix Annot39 /home/projects/cge/scratch/people/mikwol/data/Assemblies/DTU2020-371-PRJ1246-Leuconostoc-pseudomesenteroides-DK96_S8_L002_R1_001.fa
prokka --outdir GeneAnnotation40 --prefix Annot40 /home/projects/cge/scratch/people/mikwol/data/Assemblies/DTU2020-371-PRJ1246-Leuconostoc-pseudomesenteroides-DK96_S8_L003_R1_001.fa
prokka --outdir GeneAnnotation41 --prefix Annot41 /home/projects/cge/scratch/people/mikwol/data/Assemblies/DTU2020-371-PRJ1246-Leuconostoc-pseudomesenteroides-DK96_S8_L004_R1_001.fa
prokka --outdir GeneAnnotation42 --prefix Annot42 /home/projects/cge/scratch/people/mikwol/data/Assemblies/DTU2020-372-PRJ1246-Leuconostoc-pseudomesenteroides-DK102_S9_L001_R1_001.fa
prokka --outdir GeneAnnotation43 --prefix Annot43 /home/projects/cge/scratch/people/mikwol/data/Assemblies/DTU2020-372-PRJ1246-Leuconostoc-pseudomesenteroides-DK102_S9_L002_R1_001.fa
prokka --outdir GeneAnnotation44 --prefix Annot44 /home/projects/cge/scratch/people/mikwol/data/Assemblies/DTU2020-372-PRJ1246-Leuconostoc-pseudomesenteroides-DK102_S9_L003_R1_001.fa
prokka --outdir GeneAnnotation45 --prefix Annot45 /home/projects/cge/scratch/people/mikwol/data/Assemblies/DTU2020-372-PRJ1246-Leuconostoc-pseudomesenteroides-DK102_S9_L004_R1_001.fa
