#!/usr/bin/env python3

import argparse
import subprocess
import sys

parser = argparse.ArgumentParser(description="Infiles")

parser.add_argument('-i', '--infile', help=("Input files"), nargs='+', metavar='In.fasta', default=None)
args = parser.parse_args()


outfile=open('runProkka.sh','w')
print('#!/bin/sh',file=outfile)                                         #Writes jobscript
print('#PBS -A cge -W group_list=cge',file=outfile)
print('#PBS -o /dev/null -e /dev/null',file=outfile)
print('#PBS -N mikwoljob', file=outfile)
print(' ',file=outfile)
print('module purge', file=outfile)
print('module load tools', file=outfile)
print('module load perl/5.30.2', file=outfile)
print('module load clustalw/2.1', file=outfile)
print('module load prodigal/2.6.3', file=outfile)
print('module load hmmer/3.2.1', file=outfile)
print('module load ncbi-blast/2.10.0+', file=outfile)
print('module load aragorn/1.2.36', file=outfile)
print('module load tbl2asn/20200706', file=outfile)
print('module load infernal/1.1.2', file=outfile)
print('module load barrnap/0.7', file=outfile)
print('module load prokka/1.14.5', file=outfile)
print(' ',file=outfile)
print('cd','/home/projects/cge/people/rkmo/temp/mikkel/CrisPRProject/Output/Phylogeny/Core',file=outfile)

for i in range(len(args.infile)): 
    print('prokka --outdir GeneAnnotation'+str(i),'--prefix Annot'+str(i),args.infile[i], file=outfile)
outfile.close()
