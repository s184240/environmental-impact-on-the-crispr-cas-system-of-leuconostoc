#!/bin/sh
#PBS -A cge -W group_list=cge
#PBS -l nodes=1:ppn=20
#PBS -l mem=20GB
#PBS -l walltime=0:30:00
#PBS -o /dev/null -e /dev/nul -e /dev/nulll
#PBS -N mikwoljob
 
module load tools
module load perl/5.30.2
module load vmatch/2.3.0
module load clustalw/2.1
module load emboss/6.6.0
module load prodigal/2.6.3
module load macsyview/1.0.1
module load hmmer/3.2.1
module load ncbi-blast/2.10.0+
module load macsyfinder/1.0.5
module load muscle/3.8.425
module load crisprcasfinder/4.2.20
 
cd /home/projects/cge/people/rkmo/temp/mikkel/CrisPRProject/Output/Dairy
perl /services/tools/crisprcasfinder/4.2.20/CRISPRCasFinder.pl -i /home/projects/cge/scratch/people/mikwol/data/Leuconostoc_Data/Dairy/GCA_000686465.1_Leuconostoc_genomic.fna -soFile /services/tools/vmatch/2.3.0/SELECT/sel392v2.so -cas -def 'S' -gscf
perl /services/tools/crisprcasfinder/4.2.20/CRISPRCasFinder.pl -i /home/projects/cge/scratch/people/mikwol/data/Leuconostoc_Data/Dairy/GCA_000686505.1_Leuconostoc_genomic.fna -soFile /services/tools/vmatch/2.3.0/SELECT/sel392v2.so -cas -def 'S' -gscf
perl /services/tools/crisprcasfinder/4.2.20/CRISPRCasFinder.pl -i /home/projects/cge/scratch/people/mikwol/data/Leuconostoc_Data/Dairy/GCA_002072475.1_ASM207247v1_genomic.fna -soFile /services/tools/vmatch/2.3.0/SELECT/sel392v2.so -cas -def 'S' -gscf
perl /services/tools/crisprcasfinder/4.2.20/CRISPRCasFinder.pl -i /home/projects/cge/scratch/people/mikwol/data/Leuconostoc_Data/Dairy/GCA_002072495.1_ASM207249v1_genomic.fna -soFile /services/tools/vmatch/2.3.0/SELECT/sel392v2.so -cas -def 'S' -gscf
perl /services/tools/crisprcasfinder/4.2.20/CRISPRCasFinder.pl -i /home/projects/cge/scratch/people/mikwol/data/Leuconostoc_Data/Dairy/GCA_002072505.1_ASM207250v1_genomic.fna -soFile /services/tools/vmatch/2.3.0/SELECT/sel392v2.so -cas -def 'S' -gscf
perl /services/tools/crisprcasfinder/4.2.20/CRISPRCasFinder.pl -i /home/projects/cge/scratch/people/mikwol/data/Leuconostoc_Data/Dairy/GCA_002072555.1_ASM207255v1_genomic.fna -soFile /services/tools/vmatch/2.3.0/SELECT/sel392v2.so -cas -def 'S' -gscf
perl /services/tools/crisprcasfinder/4.2.20/CRISPRCasFinder.pl -i /home/projects/cge/scratch/people/mikwol/data/Leuconostoc_Data/Dairy/GCA_002072565.1_ASM207256v1_genomic.fna -soFile /services/tools/vmatch/2.3.0/SELECT/sel392v2.so -cas -def 'S' -gscf
perl /services/tools/crisprcasfinder/4.2.20/CRISPRCasFinder.pl -i /home/projects/cge/scratch/people/mikwol/data/Leuconostoc_Data/Dairy/GCA_002072575.1_ASM207257v1_genomic.fna -soFile /services/tools/vmatch/2.3.0/SELECT/sel392v2.so -cas -def 'S' -gscf
perl /services/tools/crisprcasfinder/4.2.20/CRISPRCasFinder.pl -i /home/projects/cge/scratch/people/mikwol/data/Leuconostoc_Data/Dairy/GCA_002072585.1_ASM207258v1_genomic.fna -soFile /services/tools/vmatch/2.3.0/SELECT/sel392v2.so -cas -def 'S' -gscf
perl /services/tools/crisprcasfinder/4.2.20/CRISPRCasFinder.pl -i /home/projects/cge/scratch/people/mikwol/data/Leuconostoc_Data/Dairy/GCA_002092035.1_ASM209203v1_genomic.fna -soFile /services/tools/vmatch/2.3.0/SELECT/sel392v2.so -cas -def 'S' -gscf
perl /services/tools/crisprcasfinder/4.2.20/CRISPRCasFinder.pl -i /home/projects/cge/scratch/people/mikwol/data/Leuconostoc_Data/Dairy/GCA_002092075.1_ASM209207v1_genomic.fna -soFile /services/tools/vmatch/2.3.0/SELECT/sel392v2.so -cas -def 'S' -gscf
perl /services/tools/crisprcasfinder/4.2.20/CRISPRCasFinder.pl -i /home/projects/cge/scratch/people/mikwol/data/Leuconostoc_Data/Dairy/GCA_002092165.1_ASM209216v1_genomic.fna -soFile /services/tools/vmatch/2.3.0/SELECT/sel392v2.so -cas -def 'S' -gscf
perl /services/tools/crisprcasfinder/4.2.20/CRISPRCasFinder.pl -i /home/projects/cge/scratch/people/mikwol/data/Leuconostoc_Data/Dairy/GCA_002092235.1_ASM209223v1_genomic.fna -soFile /services/tools/vmatch/2.3.0/SELECT/sel392v2.so -cas -def 'S' -gscf
perl /services/tools/crisprcasfinder/4.2.20/CRISPRCasFinder.pl -i /home/projects/cge/scratch/people/mikwol/data/Leuconostoc_Data/Dairy/GCA_002092255.1_ASM209225v1_genomic.fna -soFile /services/tools/vmatch/2.3.0/SELECT/sel392v2.so -cas -def 'S' -gscf
perl /services/tools/crisprcasfinder/4.2.20/CRISPRCasFinder.pl -i /home/projects/cge/scratch/people/mikwol/data/Leuconostoc_Data/Dairy/GCA_002092295.1_ASM209229v1_genomic.fna -soFile /services/tools/vmatch/2.3.0/SELECT/sel392v2.so -cas -def 'S' -gscf
perl /services/tools/crisprcasfinder/4.2.20/CRISPRCasFinder.pl -i /home/projects/cge/scratch/people/mikwol/data/Leuconostoc_Data/Dairy/GCA_002092355.1_ASM209235v1_genomic.fna -soFile /services/tools/vmatch/2.3.0/SELECT/sel392v2.so -cas -def 'S' -gscf
perl /services/tools/crisprcasfinder/4.2.20/CRISPRCasFinder.pl -i /home/projects/cge/scratch/people/mikwol/data/Leuconostoc_Data/Dairy/GCA_002092375.1_ASM209237v1_genomic.fna -soFile /services/tools/vmatch/2.3.0/SELECT/sel392v2.so -cas -def 'S' -gscf
perl /services/tools/crisprcasfinder/4.2.20/CRISPRCasFinder.pl -i /home/projects/cge/scratch/people/mikwol/data/Leuconostoc_Data/Dairy/GCA_002092535.1_ASM209253v1_genomic.fna -soFile /services/tools/vmatch/2.3.0/SELECT/sel392v2.so -cas -def 'S' -gscf
perl /services/tools/crisprcasfinder/4.2.20/CRISPRCasFinder.pl -i /home/projects/cge/scratch/people/mikwol/data/Leuconostoc_Data/Dairy/GCA_002092555.1_ASM209255v1_genomic.fna -soFile /services/tools/vmatch/2.3.0/SELECT/sel392v2.so -cas -def 'S' -gscf
perl /services/tools/crisprcasfinder/4.2.20/CRISPRCasFinder.pl -i /home/projects/cge/scratch/people/mikwol/data/Leuconostoc_Data/Dairy/GCA_002092635.1_ASM209263v1_genomic.fna -soFile /services/tools/vmatch/2.3.0/SELECT/sel392v2.so -cas -def 'S' -gscf
perl /services/tools/crisprcasfinder/4.2.20/CRISPRCasFinder.pl -i /home/projects/cge/scratch/people/mikwol/data/Leuconostoc_Data/Dairy/GCA_002092645.1_ASM209264v1_genomic.fna -soFile /services/tools/vmatch/2.3.0/SELECT/sel392v2.so -cas -def 'S' -gscf
perl /services/tools/crisprcasfinder/4.2.20/CRISPRCasFinder.pl -i /home/projects/cge/scratch/people/mikwol/data/Leuconostoc_Data/Dairy/GCA_003346375.1_ASM334637v1_genomic.fna -soFile /services/tools/vmatch/2.3.0/SELECT/sel392v2.so -cas -def 'S' -gscf
