#!/bin/sh

#PBS -A cge -W group_list=cge
#PBS -l nodes=1:ppn=1
#PBS -l mem=1GB walltime=0:30:00 
#PBS -N mikwoljob1
#PBS -e /dev/null -o/dev/null


cd /home/projects/cge/people/rkmo/temp/mikkel/CrisPRProject/jobscripts

python3 Writejobscript.py -i /home/projects/cge/scratch/people/mikwol/data/Leuconostoc_Data/Other/*.fna
