#!/bin/sh

#PBS -A cge -W group_list=cge
#PBS -l nodes=4:ppn=10
#PBS -l mem=4GB walltime=4:00:00
#PBS -e /dev/null -o /dev/null
#PBS -N mikwoljob

module load tools
module load anaconda3/4.4.0

cd /home/projects/cge/scratch/people/mikwol/data/

python3 /home/projects/cge/apps/FoodQCPipeline.py /home/projects/cge/scratch/people/mikwol/data/Leuconostoc_Data/raw/* --spades --trim_output /home/projects/cge/scratch/people/mikwol/data/Trimmed --qc_output /home/projects/cge/scratch/people/mikwol/data/QC 
