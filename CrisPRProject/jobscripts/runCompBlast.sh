#!/bin/sh

#PBS -A cge -W group_list=cge
#PBS -l nodes=1:ppn=1
#PBS -l mem=10GB walltime=2:00:00
#PBS -o /dev/null
#PBS -N mikwoljob

module purge
module load tools
module load perl
module load ncbi-blast/2.11.0+

cd /home/projects/cge/people/rkmo/temp/mikkel/CrisPRProject/Output/Blast/Comp/

blastn -db nt -query AllSpacersComp.fasta -out SpacerCompres.out -remote


