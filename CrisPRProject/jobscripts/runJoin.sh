#!/bin/sh

#PBS -A cge -W group_list=cge
#PBS -l nodes=1:ppn=1
#PBS -l mem=2GB walltime=0:30:00 
#PBS -N mikwoljob1
#PBS -o /dev/null

module purge
module load tools


cd /home/projects/cge/people/rkmo/temp/mikkel/CrisPRProject/

python3 joinall.py -i /home/projects/cge/scratch/people/mikwol/data/Leuconostoc_Data/Dairy/*.fna /home/projects/cge/scratch/people/mikwol/data/Leuconostoc_Data/Plants/*.fna /home/projects/cge/scratch/people/mikwol/data/Leuconostoc_Data/Other/*.fna /home/projects/cge/scratch/people/mikwol/data/Assemblies/*.fa 
