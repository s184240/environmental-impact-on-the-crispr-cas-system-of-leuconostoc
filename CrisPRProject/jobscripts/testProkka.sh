#!/bin/sh
#PBS -A cge -W group_list=cge
#PBS -o /dev/null
#PBS -N mikwoljob
 
module load tools
module load perl/5.30.2
module load clustalw/2.1
module load prodigal/2.6.3
module load hmmer/3.2.1
module load ncbi-blast/2.10.0+
module load aragorn/1.2.36
module load tbl2asn/20200706
module load infernal/1.1.2
module load barrnap/0.7
module load prokka/1.14.5

cd /home/projects/cge/people/rkmo/temp/mikkel/CrisPRProject/

prokka --outdir GeneAnnotation --prefix general  
