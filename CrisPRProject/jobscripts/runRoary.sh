#!/bin/sh
#PBS -A cge -W group_list=cge
#PBS -o /dev/null
#PBS -N mikwoljob

module purge 
module load tools
module load perl/5.30.2
module load roary/3.13.0 

cd /home/projects/cge/people/rkmo/temp/mikkel/CrisPRProject/

roary -e --mafft -f CorePhyl /home/projects/cge/people/rkmo/temp/mikkel/CrisPRProject/Output/Phylogeny/Annotation/*/*.gff

