#!/bin/sh

#PBS -A cge -W group_list=cge 
#PBS -N mikwoljob1
#PBS -o /dev/null
module purge

module load tools
module load muscle/3.8.425

cd /home/projects/cge/people/rkmo/temp/mikkel/CrisPRProject/Output

/services/tools/muscle/3.8.425/muscle3.8.425_i86linux64 -in PlantSpacers.fasta -out PlantSpacersAl.fasta 
