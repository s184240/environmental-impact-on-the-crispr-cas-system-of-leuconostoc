#!/usr/bin/env python3

import argparse
import subprocess
import sys

parser = argparse.ArgumentParser(description="Infiles")

parser.add_argument('-i', '--infile', help=("Input files"), nargs='+', metavar='In.fasta', default=None)
args = parser.parse_args()


outfile=open('runCrisprCasFinder.sh','w')
print('#!/bin/sh',file=outfile)                                         #Writes jobscript
print('#PBS -A cge -W group_list=cge',file=outfile)
print('#PBS -l nodes=1:ppn=20',file=outfile)
print('#PBS -l mem=20GB',file=outfile)
print('#PBS -l walltime=0:30:00',file=outfile)
print('#PBS -o /dev/null -e /dev/null',file=outfile)
print('#PBS -N mikwoljob', file=outfile)
print(' ',file=outfile)
print('module load tools', file=outfile)
print('module load perl/5.30.2', file=outfile)
print('module load vmatch/2.3.0', file=outfile)
print('module load clustalw/2.1', file=outfile)
print('module load emboss/6.6.0', file=outfile)
print('module load prodigal/2.6.3', file=outfile)
print('module load macsyview/1.0.1', file=outfile)
print('module load hmmer/3.2.1', file=outfile)
print('module load ncbi-blast/2.10.0+', file=outfile)
print('module load macsyfinder/1.0.5', file=outfile)
print('module load muscle/3.8.425', file=outfile)
print('module load crisprcasfinder/4.2.20', file=outfile)
print(' ',file=outfile)
print('cd','/home/projects/cge/people/rkmo/temp/mikkel/CrisPRProject/Output/Dairy',file=outfile)
for i in range(len(args.infile)): 
    print('perl /services/tools/crisprcasfinder/4.2.20/CRISPRCasFinder.pl -i',args.infile[i],'-soFile /services/tools/vmatch/2.3.0/SELECT/sel392v2.so -cas -def \'S\' -gscf', file=outfile)
outfile.close()

