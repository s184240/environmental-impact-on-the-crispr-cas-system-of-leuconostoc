#!/bin/sh

#PBS -A cge -W group_list=cge
#PBS -l nodes=1:ppn=1
#PBS -l mem=2GB walltime=0:10:00 
#PBS -N mikwoljob1

module purge

module load tools
module load perl/5.30.2
module load vmatch/2.3.0
module load clustalw/2.1
module load emboss/6.6.0
module load prodigal/2.6.3
module load macsyview/1.0.1
module load hmmer/3.2.1
module load ncbi-blast/2.10.0+
module load macsyfinder/1.0.5
module load muscle/3.8.425
module load crisprcasfinder/4.2.20

cd /home/projects/cge/people/rkmo/temp/mikkel/CrisPRProject/

perl /services/tools/crisprcasfinder/4.2.20/CRISPRCasFinder.pl -i /home/projects/cge/scratch/people/mikwol/data/Leuconostoc_Data/Dairy/*.fna -soFile /services/tools/vmatch/2.3.0/SELECT/sel392v2.so -cas -def 'S' -gscf -rcfowce



